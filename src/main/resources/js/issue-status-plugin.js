AJS.$(function(){
    if (AJS.$.fn.tooltip) {
        AJS.$(".jira-issue-status-lozenge[data-tooltip]").tooltip({
            aria: true,
            gravity: AJS.$.fn.tipsy.autoWE,
            delayIn: 100,
            html: true,
            live: true,
            title: "data-tooltip",
            className: "jira-issue-status-tooltip"
        });
    }
});
